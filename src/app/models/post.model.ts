export class Post {

    constructor(
        private _id?: number,
        private _title?: string,
        private _author?: string,
        private _creationDate?: Date,
        private _text?: string,
        private _image?: File
    ) { }

    get id() {
        return this._id;
    }

    get title() {
        return this._title;
    }

    get author() {
        return this._author;
    }

    get creationDate() {
        return this._creationDate;
    }

    get text() {
        return this._text;
    }

    get image() {
        return this._image;
    }

    set id(value: number) {
        this._id = value;
    }

    set title(value: string) {
        this._title = value;
    }

    set author(value: string) {
        this._author = value;
    }

    set creationDate(value: Date) {
        this._creationDate = value;
    }

    set text(value: string) {
        this._text = value;
    }

    set image(value: File) {
        this._image = value;
    }

}