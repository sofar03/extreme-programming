import { Component, OnInit } from '@angular/core';
import { Post } from '../models/post.model';
import { PostService } from '../services/post.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { TitleListenerService } from '../services/title-listener.service';

@Component({
  selector: 'app-blog',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {

  posts: Post[];

  searchForm: FormGroup;

  constructor(
    private blogService: PostService,
    private formBuilder: FormBuilder,
    private postService: PostService,
    private titleListenerService: TitleListenerService
  ) { }

  ngOnInit(): void {
    this.posts = this.blogService.getAllPosts();
    this.searchForm = this.formBuilder.group({
      'search': ''
    });

    this.titleListenerService.setTitle('Posts');
  }

  search() {
    console.log('search: ' + this.searchForm.value.search);
    this.posts = this.postService.searchPost(this.searchForm.value.search);
  }

}
