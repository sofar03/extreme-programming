import { TestBed } from '@angular/core/testing';

import { PostService } from './post.service';
import { Post } from '../models/post.model';

describe('PostService', () => {
  let service: PostService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ PostService ]
    });
    service = TestBed.inject(PostService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return first post', () => {
    let post = service.getPostById(1);
    expect(post.id).toBe(1);
  });

  it('should return all posts', () => {
    let posts = service.getAllPosts();
    expect(posts.length).toBeGreaterThan(1);
  });

  it('should find posts that includes \'blog\' in title', () => {
    let posts = service.searchPost('blog');
    expect(posts.length).toBeGreaterThan(1);
  });

  it('should add new post', () => {
    let post = new Post(undefined, 'test', 'test', undefined, 'test');
    service.addPost(post);
    let newPost = service.getPostById(service.getAllPosts().length - 1);

    expect(newPost.id).toBeDefined();
    expect(newPost.creationDate).toBeDefined();
  })

});
