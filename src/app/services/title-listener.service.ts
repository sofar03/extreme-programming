import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TitleListenerService {

  private _titleSource = new Subject<string>();
  $title = this._titleSource.asObservable();

  constructor() { }

  setTitle(title: string) {
    this._titleSource.next(title);
  }

}
