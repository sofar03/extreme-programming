import { TestBed } from '@angular/core/testing';

import { TitleListenerService } from './title-listener.service';

describe('TitleListenerService', () => {
  let service: TitleListenerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TitleListenerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should add new title to the source', () => {
    let testTitle = 'Test';

    service.$title.subscribe(title => {
      expect(title).toBe(testTitle);
    });
    service.setTitle(testTitle);
  })

});
