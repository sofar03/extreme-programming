import { Injectable } from '@angular/core';
import { Post } from '../models/post.model';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  posts: Post[];

  constructor() {
    this.posts = [
      new Post(1, 'Test blog 1', 'John Wick', new Date(Date.now()), 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'),
      new Post(2, 'Test blog 2', 'Unknown', new Date(Date.now()), 'Diam quam nulla porttitor massa id. Nunc scelerisque viverra mauris in aliquam sem fringilla ut morbi. Cursus euismod quis viverra nibh cras pulvinar. Risus nullam eget felis eget nunc. In fermentum et sollicitudin ac. Quis auctor elit sed vulputate mi sit. Turpis nunc eget lorem dolor. Lectus proin nibh nisl condimentum id venenatis. Sed nisi lacus sed viverra tellus in. Magna fringilla urna porttitor rhoncus dolor purus non enim praesent. Porttitor lacus luctus accumsan tortor posuere ac ut consequat. Urna nec tincidunt praesent semper feugiat.'),
      new Post(3, 'Test blog 3', 'Unknown', new Date(Date.now()), 'Eros in cursus turpis massa tincidunt dui ut ornare lectus. Massa placerat duis ultricies lacus sed turpis tincidunt id aliquet. Tempor nec feugiat nisl pretium fusce id velit ut. Sed velit dignissim sodales ut eu. Feugiat in fermentum posuere urna. Fringilla phasellus faucibus scelerisque eleifend donec pretium. Feugiat in fermentum posuere urna nec tincidunt praesent semper feugiat. Euismod in pellentesque massa placerat duis. Tristique senectus et netus et malesuada fames ac turpis egestas. Pretium nibh ipsum consequat nisl vel. Ullamcorper dignissim cras tincidunt lobortis feugiat vivamus at augue. Quam quisque id diam vel.'),
      new Post(4, 'Test blog 4', 'Unknown', new Date(Date.now()), 'Vel eros donec ac odio. Porttitor eget dolor morbi non arcu risus. Ullamcorper morbi tincidunt ornare massa eget egestas purus viverra accumsan. Viverra maecenas accumsan lacus vel facilisis volutpat est velit egestas. Hendrerit gravida rutrum quisque non tellus orci ac auctor. Pulvinar sapien et ligula ullamcorper malesuada proin libero. Ac tortor vitae purus faucibus ornare suspendisse. Magna fermentum iaculis eu non diam phasellus vestibulum. Sapien pellentesque habitant morbi tristique senectus et netus et. At tempor commodo ullamcorper a lacus.'),
      new Post(5, 'Test blog 5', 'Unknown', new Date(Date.now()), 'Et magnis dis parturient montes nascetur ridiculus. Euismod nisi porta lorem mollis aliquam ut porttitor leo. Nunc eget lorem dolor sed viverra ipsum nunc. Purus ut faucibus pulvinar elementum integer enim. Massa tincidunt nunc pulvinar sapien et ligula ullamcorper malesuada. Fames ac turpis egestas integer eget aliquet. Nunc vel risus commodo viverra maecenas accumsan lacus. Faucibus vitae aliquet nec ullamcorper sit amet. Amet volutpat consequat mauris nunc congue nisi vitae suscipit. Penatibus et magnis dis parturient montes nascetur ridiculus mus mauris. Dui nunc mattis enim ut tellus.')
    ];
  }

  addPost(post: Post) {
    post.id = this.posts[this.posts.length - 1].id + 1;
    post.creationDate = new Date(Date.now());
    this.posts.push(post);
  }

  getPostById(id: number) {
    return this.posts.find((item, index, array) => item.id === id);
  }

  getAllPosts() {
    return this.posts;
  }

  searchPost(value: string) {
    if (value === '' || value === null || value === undefined) {
      return this.posts;
    }
    return this.posts.filter(p => p.title.toLowerCase().includes(value.toLowerCase()));
  }

}
