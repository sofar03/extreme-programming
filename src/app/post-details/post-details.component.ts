import { Component, OnInit } from '@angular/core';
import { Post } from '../models/post.model';
import { Router, ActivatedRoute } from '@angular/router';
import { PostService } from '../services/post.service';
import { TitleListenerService } from '../services/title-listener.service';

@Component({
  selector: 'app-post-details',
  templateUrl: './post-details.component.html',
  styleUrls: ['./post-details.component.css']
})
export class PostDetailsComponent implements OnInit {

  post: Post;
  imageUrl: any;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private postService: PostService,
    private titleListenerService: TitleListenerService,
  ) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => {
      let blogId = Number(params['id']);

      this.post = this.postService.getPostById(blogId);
      console.log(this.post);

      this.titleListenerService.setTitle(this.post.author);

      if (this.post.image !== null, this.post.image !== undefined) {
        const reader = new FileReader();
        reader.readAsDataURL(this.post.image);
        reader.onload = (event) => {
          this.imageUrl = event.target.result;
        };
      }
    });
  }

}
