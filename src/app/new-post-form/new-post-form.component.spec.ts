import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewPostFormComponent } from './new-post-form.component';
import { DebugElement } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';

describe('NewPostFormComponent', () => {
  let component: NewPostFormComponent;
  let fixture: ComponentFixture<NewPostFormComponent>;
  let de: DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewPostFormComponent ],
      imports: [ ReactiveFormsModule, RouterTestingModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewPostFormComponent);
    component = fixture.componentInstance;
    de = fixture.debugElement;

    component.ngOnInit();

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('form invalid when empty', () => {
    expect(component.newPostForm.valid).toBeFalsy();
  });

  it('form valid', () => {
    let title = component.newPostForm.controls['title'];
    let author = component.newPostForm.controls['author'];
    let text = component.newPostForm.controls['text'];

    title.setValue('Test');
    author.setValue('Test');
    text.setValue('Test');

    expect(component.newPostForm.valid).toBeTruthy();
  })

});
