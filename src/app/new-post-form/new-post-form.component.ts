import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Post } from '../models/post.model';
import { PostService } from '../services/post.service';
import { TitleListenerService } from '../services/title-listener.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-new-post-form',
  templateUrl: './new-post-form.component.html',
  styleUrls: ['./new-post-form.component.css']
})
export class NewPostFormComponent implements OnInit {

  newPostForm: FormGroup;

  postImage: File;

  imageBtnName: string = "Upload image";

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private postService: PostService,
    private titleListenerService: TitleListenerService,
  ) { }

  ngOnInit(): void {
    this.newPostForm = this.formBuilder.group({
      'title': ['', [
        Validators.required,
        Validators.minLength(3)
      ]],
      'author': ['unknown', [
        Validators.required,
        Validators.minLength(3)
      ]],
      'text': ['', [
        Validators.required,
        Validators.minLength(3)
      ]]
    });

    this.titleListenerService.setTitle("New post");
  }

  newPost() {
    if (this.newPostForm.valid) {
      const newPost = new Post(
        null, // id
        this.newPostForm.value.title,
        this.newPostForm.value.author,
        null, // creationDate
        this.newPostForm.value.text,
        this.postImage);

      console.log(newPost);

      this.postService.addPost(newPost);
      this.router.navigateByUrl('/');
    } else {
      console.log("Invalid post data");
    }
  }

  upload() {
    const fileInput = document.getElementById('fileUpload') as HTMLInputElement;
    fileInput.click();

    fileInput.onchange = () => {
      this.postImage = fileInput.files[0];
      this.imageBtnName = this.postImage.name;
    }
  }

}
