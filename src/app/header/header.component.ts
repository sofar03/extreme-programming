import { Component, OnInit } from '@angular/core';
import { TitleListenerService } from '../services/title-listener.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  title: string = "Extreme project";

  constructor(
    private titleListenerService: TitleListenerService
  ) { }

  ngOnInit(): void {
    this.titleListenerService.$title.subscribe(title => {
      this.title = title;
    });
  }

}
