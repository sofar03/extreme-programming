import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PostComponent } from './post/post.component';
import { PostDetailsComponent } from './post-details/post-details.component';
import { NewPostFormComponent } from './new-post-form/new-post-form.component';


const routes: Routes = [
  { path: '', component: PostComponent },
  { path: 'posts/:id', component: PostDetailsComponent },
  { path: 'posts/func/new', component: NewPostFormComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
